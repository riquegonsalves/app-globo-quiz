package co.rique.globoquiz.classes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.ByteArrayOutputStream;

import co.rique.globoquiz.database.DataBase;
import co.rique.globoquiz.tables.Contacts;

/**
 * Created by Phil on 22/03/2015.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    private Context contextObj;

    Bitmap bitmap = null;
    private String encodedString;
    RequestParams params = new RequestParams();
    private String uploadServerUri = "http://www.rique.co/globoquiz/upload.php";

    @Override
    public void onReceive(final Context context, final Intent intent) {

        contextObj = context;

        int status = NetworkUtil.getConnectivityStatusString(context);

        Log.e("Phil", "Network Receiver Started");

        if (!"android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            if(status==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
                //Toast.makeText(context, "Sem Conexão", Toast.LENGTH_SHORT).show();
                Log.d("Phil","Sem Conexão");
            }else{
                Log.d("Phil", "Tem Conexão");


                PhotoHandler photoHandler = new PhotoHandler(context);

                // Inicia a busca de um contato para enviar
                photoHandler.init();

                /*Log.d("Phil", "Dump Init");

                DatabaseUtils.dumpCursor(contacts);

                Log.d("Phil", "Dump End");

                contacts.move(-1);

                while(contacts.moveToNext()){

                    String email = contacts.getString(1);
                    Integer id = Integer.valueOf(contacts.getString(0));
                    String file = contacts.getString(2);
                    String statusT = contacts.getString(3);
                    Log.d("Phil", "Email: " + email);
                    Log.d("Phil", "File: " + file);
                    Log.d("Phil", "Status: " + String.valueOf(statusT));

                    encodeImagetoString(file, email, id);
                }*/
            }
        }
    }

    public void encodeImagetoString(final String filePath, final String email, final Integer id) {

        new AsyncTask<Void, Void, String>() {

            protected void onPreExecute() {

            };

            @Override
            protected String doInBackground(Void... params) {

                BitmapFactory.Options options = null;
                options = new BitmapFactory.Options();
                options.inSampleSize = 1;

                bitmap = BitmapFactory.decodeFile(filePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Must compress the Image to reduce image size to make upload easy
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                byte[] byte_arr = stream.toByteArray();
                // Encode Image to String
                encodedString = Base64.encodeToString(byte_arr, 0);
                return "";

            }

            @Override
            protected void onPostExecute(String msg) {

                Log.d("Phil", "Calling Upload");
                //prgDialog.setMessage("Calling Upload");
                // Put converted Image string into Async Http Post param
                params.put("image", encodedString);
                params.put("imagePath", filePath);

                Log.d("Email Post Execute: ", email);
                params.put("email", email);

                Contacts contact = new Contacts();
                contact.setId(id);
                contact.setFile(filePath);
                contact.setEmail(email);

                // Trigger Image upload
                triggerImageUpload(contact);
            }
        }.execute(null, null, null);
    }

    public void triggerImageUpload(Contacts contact) {
        makeHTTPCall(contact);
    }

    // Make Http call to upload Image to Php server
    public void makeHTTPCall(final Contacts contact) {

        Log.d("Phil", "Chamando o PHP");

        AsyncHttpClient client = new AsyncHttpClient();
        // Don't forget to change the IP address to your LAN address. Port no as well.
        client.post(uploadServerUri, params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http
            // response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                //prgDialog.hide();
                Log.d("Phil", "Response: "+response);

                Integer status = 1;

                if(response != "ok"){
                    status = 0;
                }
                // 0 deu erro ao enviar
                // 1 Ok ao enviar
                contact.setStatus(status);

                // Database
                DataBase dataBase = new DataBase(contextObj);
                dataBase.updateContact(contact);
            }

            // When the response returned by REST has Http
            // response code other than '200' such as '404',
            // '500' or '403' etc
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                // Hide Progress Dialog
                //prgDialog.hide();
                // When Http response code is '404'
                if (statusCode == 404) {
                    Log.d("Phil", "Requested resource not found");

                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Log.d("Phil", "Something went wrong at server end");
                }
                // When Http response code other than 404, 500
                else {
                    Log.d("Phil", "Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "+ statusCode);
                    Log.d("Phil", "Content: "+ content);
                }
            }
        });
    }
}

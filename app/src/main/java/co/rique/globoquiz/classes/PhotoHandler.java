package co.rique.globoquiz.classes;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.ByteArrayOutputStream;

import co.rique.globoquiz.database.DataBase;
import co.rique.globoquiz.tables.Contacts;

public class PhotoHandler {

    private Context context;
    Bitmap bitmap = null;
    private String encodedString;
    RequestParams params = new RequestParams();
    private String uploadServerUri = "http://formigadesign.kinghost.net/globoquiz/upload.php";

    public PhotoHandler(final Context context){
        this.context = context;
    }

    public void init() {

        Log.d("Phil", "Init function from PhotoHandler");

        // Reset
        this.bitmap = null;
        this.encodedString = null;
        this.params = new RequestParams();

        // Ini
        Contacts contact = this.getContacts();

        if(contact.getId() != null){
            encodeImagetoString(contact);
        }
    }

    public void encodeImagetoString(final Contacts contact) {

        new AsyncTask<Void, Void, String>() {

            protected void onPreExecute() {};

            @Override
            protected String doInBackground(Void... params) {

                BitmapFactory.Options options = null;
                options = new BitmapFactory.Options();
                options.inSampleSize = 1;

                bitmap = BitmapFactory.decodeFile(contact.getFile(), options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Must compress the Image to reduce image size to make upload easy
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                byte[] byte_arr = stream.toByteArray();
                // Encode Image to String
                encodedString = Base64.encodeToString(byte_arr, 0);
                return "";

            }

            @Override
            protected void onPostExecute(String msg) {

                Log.d("Phil", "Calling Upload");
                //prgDialog.setMessage("Calling Upload");
                // Put converted Image string into Async Http Post param
                params.put("image", encodedString);
                params.put("imagePath", contact.getFile());

                Log.d("Email Post Execute: ", contact.getEmail());
                params.put("email", contact.getEmail());

                // Trigger Image upload
                makeHTTPCall(contact);
            }
        }.execute(null, null, null);
    }

    public void makeHTTPCall(final Contacts contact) {

        Log.d("Phil", "Chamando o PHP");

        AsyncHttpClient client = new AsyncHttpClient();
        // Don't forget to change the IP address to your LAN address. Port no as well.
        client.post(uploadServerUri, params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http
            // response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                //prgDialog.hide();
                Log.d("Phil", "Response: "+response);

                Integer status = 1;

                if(!response.equals("ok")){
                    status = 0;
                }

                Log.d("Phil", "Check Status with connection and Ok response: " + status);
                // 0 deu erro ao enviar
                // 1 Ok ao enviar
                contact.setStatus(status);

                // Database
                DataBase dataBase = new DataBase(context);
                dataBase.updateContact(contact);

                init();
            }

            // When the response returned by REST has Http
            // response code other than '200' such as '404',
            // '500' or '403' etc
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                // Hide Progress Dialog
                //prgDialog.hide();
                // When Http response code is '404'
                if (statusCode == 404) {
                    Log.d("Phil", "Requested resource not found");

                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Log.d("Phil", "Something went wrong at server end");
                }
                // When Http response code other than 404, 500
                else {
                    Log.d("Phil", "Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "+ statusCode);
                    Log.d("Phil", "Content: "+ content);
                }
            }
        });
    }

    private Contacts getContacts(){

        Log.d("Phil", "getContacts function from PhotoHandler");

        DataBase dataBase = new DataBase(context);
        Cursor contacts = dataBase.getContacts();

        Contacts contact = new Contacts();

        if(contacts.getCount() > 0){
            contacts.move(-1);

            while(contacts.moveToNext()){

                Log.d("Phil", "Email: " + contacts.getString(1));
                Log.d("Phil", "File: " + contacts.getString(2));
                Log.d("Phil", "Status: " + String.valueOf(contacts.getString(3)));

                contact.setEmail(contacts.getString(1));
                contact.setFile(contacts.getString(2));
                contact.setStatus(Integer.valueOf(contacts.getString(3)));
                contact.setId(Integer.valueOf(contacts.getString(0)));
            }
        }

        Log.d("Phil", "Contact ID: " + contact.getId());
        Log.d("Phil", "Dump Init");
        Log.d("Phil", "Cursor Count: " + contacts.getCount());

        DatabaseUtils.dumpCursor(contacts);

        Log.d("Phil", "Dump End");

        return contact;
    }
}

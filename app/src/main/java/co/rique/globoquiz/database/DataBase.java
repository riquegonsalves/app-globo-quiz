package co.rique.globoquiz.database;

/**
 * Created by Phil on 22/03/2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import co.rique.globoquiz.tables.Contacts;

/**
 * Created by Alex on 27/11/2014.
 */
public class DataBase {

    private SQLiteDatabase database;

    public DataBase(Context context){

        DataBaseCore dataBaseCore = new DataBaseCore(context);
        this.database = dataBaseCore.getWritableDatabase();
    }

    public void insertContactInfos(Contacts contact){

        ContentValues contentValues = new ContentValues();
        contentValues.put("email", contact.getEmail());
        contentValues.put("file", contact.getFile());
        contentValues.put("status", contact.getStatus());

        this.database.insert("contacts", null, contentValues);
    }

    /**
     * Busca todos os contatos de ajuda cadastrados
     * @return
     */
    public Cursor getContacts(){

        String columns[] = new String[]{"_id", "email", "file", "status"};
        //return this.database.query("contacts", columns, null, null, null, null, null);
        return this.database.query("contacts", columns, "status = 0", null, null, null, "_id ASC", "1");
    }

    public void updateContact(Contacts contact){

        ContentValues contentValues = new ContentValues();
        contentValues.put("email", contact.getEmail());
        contentValues.put("status", contact.getStatus());
        contentValues.put("file", contact.getFile());

        this.database.update("contacts", contentValues, "_id = "+ String.valueOf(contact.getId()), null);
    }
}

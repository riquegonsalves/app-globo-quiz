package co.rique.globoquiz.database;

/**
 * Created by Phil on 22/03/2015.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Phil on 27/11/2014.
 */
public class DataBaseCore extends SQLiteOpenHelper{

    /**
     * Database Name (pro_mulher)
     */
    private static final String DATABASE_NAME = "globo_quiz";

    /**
     * Database Version (pro_mulher)
     */
    private static final int DATABASE_VERSION = 2;

    public DataBaseCore(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Cria a tabela do usuário
        db.execSQL("CREATE TABLE contacts (_id INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(255) NULL, status INTEGER NULL, file VARCHAR(255) NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Essa função é chamada quando a versão do banco de dados é mudada
        db.execSQL("DROP TABLE contacts");

        // Cria denovo
        onCreate(db);
    }
}
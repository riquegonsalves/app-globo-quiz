package co.rique.globoquiz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.ByteArrayOutputStream;
import java.util.regex.Pattern;

import co.rique.globoquiz.classes.NetworkUtil;
import co.rique.globoquiz.database.DataBase;
import co.rique.globoquiz.tables.Contacts;

public class Twelfth extends Activity {

    private View decorView;

    private String filePath;
    private Bitmap bitmap;
    private String encodedString;
    RequestParams params = new RequestParams();
    private String uploadServerUri = "http://formigadesign.kinghost.net/globoquiz/upload.php";
    private Contacts contact;

    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.decorView = getWindow().getDecorView();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twelfth);

        Bundle extras = getIntent().getExtras();
        this.filePath = extras.getString("FILE_PATH");

        // Cria o tipo da fonte
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/arial_rounded_mt_bold.ttf");

        // Busca o text field da primeira tela
        TextView tv = (TextView) findViewById(R.id.text_12_1);

        // Aplica a fonte no text view
        tv.setTypeface(tf);

        //

        //
        ImageButton enviar_por_email = (ImageButton) findViewById(R.id.enviar_por_email);

        enviar_por_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer nu = NetworkUtil.getConnectivityStatusString(Twelfth.this);
                Log.d("Phil", "Network Return: " + nu);

                EditText emailField = (EditText) findViewById(R.id.emailField);

                String email = emailField.getText().toString();

                if(checkEmail(email)){

                    contact = new Contacts();
                    contact.setEmail(email);
                    contact.setFile(filePath);

                    // Não tem conexão
                    // Salva no banco com status 0
                    // Tem conexão
                    // Salva no banco com status 1
                    // Envia o Email
                    Integer status = (nu == 0)? 0: 1;
                    contact.setStatus(status);

                    if(nu == 0){

                        DataBase dataBase = new DataBase(Twelfth.this);
                        dataBase.insertContactInfos(contact);

                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        Intent intent = new Intent(Twelfth.this, First.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                },
                                1000);
                    }else{
                        encodeImagetoString();
                    }

                        Toast toast = Toast.makeText(Twelfth.this, "Em breve sua foto será encaminhada para seu e-mail! Obrigado.", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.BOTTOM, 0, 70);

                        LinearLayout toastLayout = (LinearLayout) toast.getView();

                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();

                }else{

                    emailField.requestFocus();

                    Toast toast = Toast.makeText(Twelfth.this, "E-mail inválido.", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.BOTTOM, 0, 70);

                    LinearLayout toastLayout = (LinearLayout) toast.getView();

                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(30);
                    toast.show();
                }
            }
        });
    }

    public void encodeImagetoString() {

        new AsyncTask<Void, Void, String>() {

            protected void onPreExecute() {

            };

            @Override
            protected String doInBackground(Void... params) {

                BitmapFactory.Options options = null;
                options = new BitmapFactory.Options();
                options.inSampleSize = 1;

                bitmap = BitmapFactory.decodeFile(filePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Must compress the Image to reduce image size to make upload easy
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                byte[] byte_arr = stream.toByteArray();
                // Encode Image to String
                encodedString = Base64.encodeToString(byte_arr, 0);
                return "";

            }

            @Override
            protected void onPostExecute(String msg) {

                Log.d("Phil", "Calling Upload");
                //prgDialog.setMessage("Calling Upload");
                // Put converted Image string into Async Http Post param
                params.put("image", encodedString);
                params.put("imagePath", filePath);

                Log.d("Phil", contact.getEmail());
                params.put("email", contact.getEmail());

                // Trigger Image upload
                triggerImageUpload();
            }
        }.execute(null, null, null);
    }

    public void triggerImageUpload() {
        makeHTTPCall();
    }

    // Make Http call to upload Image to Php server
    public void makeHTTPCall() {

        Log.d("Phil", "Chamando o PHP");

        AsyncHttpClient client = new AsyncHttpClient();
        // Don't forget to change the IP address to your LAN address. Port no as well.
        client.post(uploadServerUri, params, new AsyncHttpResponseHandler() {
                    // When the response returned by REST has Http
                    // response code '200'
                    @Override
                    public void onSuccess(String response) {
                        // Hide Progress Dialog
                        //prgDialog.hide();
                        Log.d("Phil", "Response: "+response);

                        Integer status = 1;

                        if(!response.equals("ok")){
                            status = 0;
                        }

                        Log.d("Phil", "Check Status with connection and Ok response: " + status);
                        // 0 deu erro ao enviar
                        // 1 Ok ao enviar
                        contact.setStatus(status);

                        // Database
                        DataBase dataBase = new DataBase(Twelfth.this);
                        dataBase.insertContactInfos(contact);

                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        Intent intent = new Intent(Twelfth.this, First.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                },
                                1000);
                    }

                    // When the response returned by REST has Http
                    // response code other than '200' such as '404',
                    // '500' or '403' etc
                    @Override
                    public void onFailure(int statusCode, Throwable error, String content) {
                        // Hide Progress Dialog
                        //prgDialog.hide();
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Log.d("Phil", "Requested resource not found");

                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Log.d("Phil", "Something went wrong at server end");
                        }
                        // When Http response code other than 404, 500
                        else {
                            Log.d("Phil", "Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "+ statusCode);
                        }
                    }
                });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        if (hasFocus) {
            decorView.setSystemUiVisibility(flags);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_twelfth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

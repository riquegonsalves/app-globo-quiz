package co.rique.globoquiz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class Fourth extends Activity {

    private View decorView;

    private static String TAG = "Fourth Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {



        this.decorView = getWindow().getDecorView();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);


        // Cria o tipo da fonte
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/arial_rounded_mt_bold.ttf");

        // Busca o text field da primeira tela
        TextView tv = (TextView) findViewById(R.id.text_4_1);

        // Aplica a fonte no text view
        tv.setTypeface(tf);
    }

    public void selectFirst(View view){

        int button_id = view.getId();

        switch (button_id){
            case (R.id.opt_1_1):
                // Errado
                this.changeAnswer(R.id.image_1_1, false);
                break;
            case (R.id.opt_1_2):
                // Errado
                this.changeAnswer(R.id.image_1_2, false);
                break;
            case (R.id.opt_1_3):
                // Certo
                this.changeAnswer(R.id.image_1_3, true);
                // Manda pra outra view
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                Intent intent = new Intent(Fourth.this, Fifth.class);
                                startActivity(intent);
                                finish();
                            }
                        },
                        1000);
                break;
            case (R.id.opt_1_4):
                // Errado
                this.changeAnswer(R.id.image_1_4, false);
                break;
        }


    }

    private void changeAnswer(int id, boolean correct){

        ImageView image = (ImageView) findViewById(id);

        if(!correct){
            image.setImageResource(R.drawable.x_q);

            Toast toast = Toast.makeText(Fourth.this, "Resposta errada. Tente outra alternativa!", Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.BOTTOM, 0, 70);

            LinearLayout toastLayout = (LinearLayout) toast.getView();

            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();

        }else{
            image.setImageResource(R.drawable.check_q);

            Toast toast = Toast.makeText(Fourth.this, "Resposta correta, aguarde a próxima questão!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.BOTTOM, 0, 70);

            LinearLayout toastLayout = (LinearLayout) toast.getView();

            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);

            toast.show();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        if (hasFocus) {
            decorView.setSystemUiVisibility(flags);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fourth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package co.rique.globoquiz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

public class Tenth extends Activity {

    private SurfaceView preview=null;
    private SurfaceHolder previewHolder=null;
    private Camera camera=null;
    private boolean inPreview=false;
    private boolean cameraConfigured=false;
    public String filePath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenth);

        Toast toast = Toast.makeText(Tenth.this, "Toque na tela para tirar foto.", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.BOTTOM, 0, 70);

        LinearLayout toastLayout = (LinearLayout) toast.getView();

        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toastTV.setTextSize(30);
        toast.show();

        preview=(SurfaceView)findViewById(R.id.preview);
        previewHolder=preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            Camera.CameraInfo info=new Camera.CameraInfo();

            for (int i=0; i < Camera.getNumberOfCameras(); i++) {
                Camera.getCameraInfo(i, info);

                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    camera=Camera.open(i);
                }
            }
        }

        if (camera == null) {
            camera=Camera.open();
        }

        startPreview();
    }

    @Override
    public void onPause() {
        if (inPreview) {
            camera.stopPreview();
        }

        camera.release();
        camera=null;
        inPreview=false;

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menu_tenth, menu);

        return(super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {

        }

        return(super.onOptionsItemSelected(item));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        File file = new File(Environment.getExternalStorageDirectory() + "/globo_quiz");

        if(!file.exists()){
            file.mkdirs();
        }

        filePath = Environment.getExternalStorageDirectory() + "/globo_quiz/screenshot_"+ts+".jpg";

        if (inPreview) {
            camera.takePicture(null, null, photoCallback);
            inPreview=false;
        }


        return true;
    }

    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result=null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result=size;
                }
                else {
                    int resultArea=result.width * result.height;
                    int newArea=size.width * size.height;

                    if (newArea > resultArea) {
                        result=size;
                    }
                }
            }
        }

        return(result);
    }

    private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
        Camera.Size result=null;

        for (Camera.Size size : parameters.getSupportedPictureSizes()) {
            if (result == null) {
                result=size;
            }
            else {
                int resultArea=result.width * result.height;
                int newArea=size.width * size.height;

                if (newArea < resultArea) {
                    result=size;
                }
            }
        }

        return(result);
    }

    private void initPreview(int width, int height) {

        if (camera != null && previewHolder.getSurface() != null) {
            try {
                camera.setPreviewDisplay(previewHolder);
            }
            catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback", "Exception in setPreviewDisplay()", t);
                Toast.makeText(Tenth.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }

            if (!cameraConfigured) {
                Camera.Parameters parameters=camera.getParameters();
                Camera.Size size=getBestPreviewSize(width, height, parameters);
                Camera.Size pictureSize=getSmallestPictureSize(parameters);

                if (size != null && pictureSize != null) {
                    parameters.setPreviewSize(size.width, size.height);
                    parameters.setRotation(0);
                    //parameters.setPictureSize(pictureSize.width, pictureSize.height);
                    parameters.setPictureSize(size.width, size.height);
                    parameters.setPictureFormat(ImageFormat.JPEG);

                    camera.setParameters(parameters);
                    cameraConfigured=true;
                }
            }
        }
    }

    private void startPreview() {

        if (cameraConfigured && camera != null) {
            camera.startPreview();
            inPreview=true;
        }

    }

    SurfaceHolder.Callback surfaceCallback=new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            // no-op -- wait until surfaceChanged()
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            initPreview(width, height);
            startPreview();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op
        }
    };

    Camera.PictureCallback photoCallback=new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            new SavePhotoTask().execute(data);
            camera.startPreview();
            inPreview=true;
        }
    };

    public void goTo(){

        Intent intent = new Intent(Tenth.this, Eleventh.class);
        intent.putExtra("FILE_PATH", filePath); //Put your id to your next Intent

        startActivity(intent);
        finish();
    }

    /**
     *
     * @param bmp1 O Fundo
     * @param bmp2 A foto
     * @param bmp3 O logo da globo
     * @return
     */
    private Bitmap overlay(Bitmap bmp1, Bitmap bmp2, Bitmap bmp3) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);

        Matrix matrix_2 = new Matrix();
        Matrix matrix_3 = new Matrix();

        // Foto
        matrix_2.postTranslate(100,15);
        // Logo
        matrix_3.postTranslate(620, 380);

        canvas.drawBitmap(bmp2, matrix_2, null);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp3, matrix_3, null);
        return bmOverlay;
    }

    class SavePhotoTask extends AsyncTask<byte[], String, String> {

        @Override
        protected String doInBackground(byte[]... jpeg) {

            File photo= new File(filePath);

            if (photo.exists()) {
                photo.delete();
            }

            try {
                FileOutputStream fos=new FileOutputStream(photo.getPath());
                fos.write(jpeg[0]);
                fos.close();

                Bitmap bmp1 = BitmapFactory.decodeResource(getResources(), R.drawable.moldura_nova);
                Bitmap bmp2 = BitmapFactory.decodeFile(filePath);
                Bitmap bmp3 = BitmapFactory.decodeResource(getResources(), R.drawable.logo_inicial);

                Bitmap result = overlay(bmp1, bmp2, bmp3);

                String newPhoto = filePath;
                File file = new File(newPhoto);

                if(file.exists()){
                    file.delete();
                }

                try {
                    FileOutputStream out = new FileOutputStream(file);
                    result.compress(Bitmap.CompressFormat.PNG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            catch (java.io.IOException e) {
                Log.e("PictureDemo", "Exception in photoCallback", e);
            }

            return(null);
        }

        @Override
        protected void onPostExecute(String result) {
            goTo();
        }
    }
}
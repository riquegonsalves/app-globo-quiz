package co.rique.globoquiz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


public class Ninth extends Activity {
    private View decorView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.decorView = getWindow().getDecorView();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ninth);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/arial_rounded_mt_bold.ttf");

        // Busca o text field da primeira tela
        TextView tv = (TextView) findViewById(R.id.text_9_1);
        TextView tv_2 = (TextView) findViewById(R.id.text_9_2);

        // Aplica a fonte no text view
        tv.setTypeface(tf);
        tv_2.setTypeface(tf);

        //
        ImageButton tirar_foto = (ImageButton) findViewById(R.id.tirar_foto);

        tirar_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Ninth.this, Tenth.class);
                startActivity(intent);
                finish();
            }
        });

        //
        ImageButton encerrar = (ImageButton) findViewById(R.id.encerrar);

        encerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Ninth.this, First.class);
                startActivity(intent);
                finish();
            }
        });
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        if (hasFocus) {
            decorView.setSystemUiVisibility(flags);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ninth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
